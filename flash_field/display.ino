// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Display

#include <Adafruit_SSD1306.h>
#include <Wire.h>

#define SCREEN_WIDTH 128     // OLED display width, in pixels
#define SCREEN_HEIGHT 64     // OLED display height, in pixels
#define OLED_RESET -1        // Reset pin # (or -1 if sharing Arduino reset pin)
#define SCREEN_ADDRESS 0x3C  ///< See datasheet for Address; 0x3D for 128x64, 0x3C for 128x32
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

void display_setup() {
  // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if (!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for (;;)
      ;  // Don't proceed, loop forever
  }
  display.clearDisplay();
  display.setTextSize(1);               // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);  // Draw white text
  display.cp437(true);                  // Use full 256 char 'Code Page 437' font

  display_startup_message();
  delay(500);
  display.clearDisplay();
}

void display_startup_message() {
  display.clearDisplay();
  display.setCursor(0, 0);

  display.println(F("Flash Field, 2024"));
  display.println();
  display.println(F("Studio"));
  display.println(F("Tobias Zehntner"));

  display.display();
}

void display_update() {
  display.clearDisplay();
  display.setCursor(0, 0);
  if (mode == 'c') {
    display_utc_time();
  }

  display_mode();
  display_setting();

  if (mode == 'c') {
    display_config();
  } else {
    display_next_trigger_time();
  }

  display.display();
  is_display_outdated = false;
}

void display_mode() {
  // display.println("Mode:");
  display.setTextSize(2);
  switch (mode) {
    case 'c':
      // display.println("Conf ");
      break;
    case 'd':
      display.print("Demo ");

      break;
    case 'r':
      display.print("Run  ");
      break;
  }
  if (setting_unit_number < 10) {
    display.print(" ");
  }
  display.print(setting_unit_number);
  display.print("/");
  if (setting_total_units < 10) {
    display.print(" ");
  }
  display.println(setting_total_units);
  display.setTextSize(1);
}

void display_setting() {
  // display.println("Setting: ");
  display.setTextSize(2);
  if (setting_period_time.hh) {
    display.print(setting_period_time.hh);
    display.print("h");
  }
  if (setting_period_time.mm) {
    display.print(setting_period_time.mm);
    display.print("m");
  }

  display.print("/");

  if (setting_interval_time.hh) {
    display.print(setting_interval_time.hh);
    display.print("h");
  }
  if (setting_interval_time.mm) {
    display.print(setting_interval_time.mm);
    display.print("m");
  }
  display.print(setting_interval_time.ss);
  display.print("s");

  display.println();
  display.setTextSize(1);
}

void display_utc_time() {
  if (is_gps_available) {
    display.print("UTC ");
    display_time(utc_time);
    display.println();
  } else {
    display.println("Waiting GPS...");
  }
}

void display_next_trigger_time() {
  if (is_flash_ready) {
    display.print("Next: ");
    struct time countdown_time = time_diff(utc_time, next_trigger_time);
    if (countdown_time.hh) {
      display.print(countdown_time.hh);
      display.print("h");
    }
    if (countdown_time.mm) {
      display.print(countdown_time.mm);
      display.print("m");
    }
    display.print(countdown_time.ss);
    display.print("s");
    display.println();
  } else if (!is_gps_available) {
    display.println("Waiting GPS...");
  }
}

void display_time(struct time t) {
  if (t.hh < 10) {
    display.print(0);
  }
  display.print(t.hh, DEC);
  display.print(":");
  if (t.mm < 10) {
    display.print(0);
  }
  display.print(t.mm, DEC);
  display.print(":");
  if (t.ss < 10) {
    display.print(0);
  }
  display.print(t.ss, DEC);
}

void display_config() {
  switch (config_step) {
    case 1:
      display.println("Set Total Units");
      break;
    case 2:
      display.println("Set Unit Number");
      break;
    case 3:
      display.println("Set Period");
      break;
    case 4:
      display.println("Set Interval");
      break;
    case 5:
      display.println("ENTER to Save");
      break;
    default:
      display.println("ENTER to config");
      break;
  }
}
