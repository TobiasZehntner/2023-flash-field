// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Config

byte const MAX_SETTING_TOTAL_UNITS = 50;
byte const MIN_SETTING_TOTAL_UNITS = 2;
unsigned const int MAX_SETTING_PERIOD_MIN = 24 * 60;
unsigned const int MIN_SETTING_PERIOD_MIN = 1;
byte const FLASH_CHARGE_TIME_SEC = 11;
byte const NUM_CONFIG_STEPS = 5;

void config_setup() {
  setting_period_time = seconds_to_time(setting_period_min * 60L);
  setting_interval_time = seconds_to_time(setting_interval_sec);
}

void iterate_config_step() {
  if (config_step >= NUM_CONFIG_STEPS) {
    // TODO save config
    config_step = 0;
  } else {
    config_step++;
  }
}

void iterate_config(int iterator) {
  switch (config_step) {
    case 1:
      iterate_setting_total_units(iterator);
      break;
    case 2:
      iterate_setting_unit_number(iterator);
      break;
    case 3:
      iterate_setting_period_min(iterator);
      break;
    case 4:
      iterate_setting_interval_sec(iterator);
      break;
  }
}

unsigned long iterate_value(unsigned long value, int iterator, int min, unsigned long max) {
  unsigned long new_value = value + iterator;
  if (new_value < min) {
    new_value = max;
  } else if (new_value > max) {
    new_value = min;
  }
  return new_value;
}

void iterate_setting_total_units(int iterator) {
  setting_total_units = iterate_value(setting_total_units, iterator, MIN_SETTING_TOTAL_UNITS, MAX_SETTING_TOTAL_UNITS);
  if (setting_unit_number > setting_total_units) {
    setting_unit_number = setting_total_units;
  }
}

void iterate_setting_unit_number(int iterator) {
  setting_unit_number = iterate_value(setting_unit_number, iterator, 1, setting_total_units);
}

void iterate_setting_period_min(int iterator) {
  while (true) {
    setting_period_min = iterate_value(setting_period_min, iterator, MIN_SETTING_PERIOD_MIN, MAX_SETTING_PERIOD_MIN);
    if (is_setting_period_min_valid(setting_period_min)) {
      setting_period_time = seconds_to_time(setting_period_min * 60L);
      break;
    }
  }
  if (!is_setting_interval_sec_valid(setting_interval_sec)) {
    setting_interval_sec = FLASH_CHARGE_TIME_SEC;
    setting_interval_time = seconds_to_time(setting_interval_sec);
  }
}

void iterate_setting_interval_sec(int iterator) {
  // TODO consider "Loading..." screen as it can take 1-2s to find large valid numbers
  unsigned long max_interval_sec = setting_period_min * 60L / 2;
  while (true) {
    setting_interval_sec = iterate_value(setting_interval_sec, iterator, FLASH_CHARGE_TIME_SEC, max_interval_sec);
    Serial.println(setting_interval_sec);
    if (is_setting_interval_sec_valid(setting_interval_sec)) {
      setting_interval_time = seconds_to_time(setting_interval_sec);
      break;
    }
  }
}

bool is_setting_period_min_valid(unsigned int minutes) {
  // Requirement: divisible in 24h
  return MAX_SETTING_PERIOD_MIN % minutes == 0;
}
bool is_setting_interval_sec_valid(unsigned int seconds) {
  // Requirement: divisible evenly in period
  // ((setting_period_min * 60) / setting_interval_sec) % 2 == 0
  float quotient = (float(setting_period_min) * float(60)) / float(seconds);
  if (quotient == int(quotient) && int(quotient) % 2 == 0) {
    return true;
  } else {
    return false;
  }
}
