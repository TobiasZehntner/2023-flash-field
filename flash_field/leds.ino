// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// LEDs


bool led_state_button_mode;
bool prev_led_state_button_mode = 0;

void led_setup() {
  digitalWrite(PIN_BUTTON_MODE_LED, HIGH);
}

void led_handler() {
  set_button_leds();
}

void set_button_leds() {
  if (is_gps_available) {
    set_led_button_mode(true);
  } else {
    // FIXME LED flickers when no GPS and using rotary knob
    // Blink Mode Switch while searching for GPS
    if ((current_millis - prev_led_state_button_mode) > 1000) {
      led_state_button_mode = !led_state_button_mode;
      set_led_button_mode(led_state_button_mode);
      prev_led_state_button_mode = current_millis;
    }
  }
}
void set_led_button_mode(bool on) {
  if (on) {
    digitalWrite(PIN_BUTTON_MODE_LED, LOW);
  } else {
    digitalWrite(PIN_BUTTON_MODE_LED, HIGH);
  }
}