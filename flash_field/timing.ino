// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Timing

struct time increase_time(struct time t, int num_seconds) {
  for (int i = 0; i < num_seconds; i++) {
    if (t.ss == 59) {
      t.ss = 0;
      if (t.mm == 59) {
        t.mm = 0;
        if (t.hh == 23) {
          t.hh = 0;
        } else {
          t.hh++;
        }
      } else {
        t.mm++;
      }
    } else {
      t.ss++;
    }
  }
  return t;
}

unsigned long time_to_seconds(struct time t) {
  return t.hh * 3600L + t.mm * 60L + t.ss;
}

struct time seconds_to_time(unsigned long ss) {
  struct time t;
  t.hh = ss / 3600L;
  t.mm = (ss - (t.hh * 3600L)) / 60;
  t.ss = (ss - (t.hh * 3600L) - (t.mm * 60));
  return t;
}

struct time time_diff(struct time from_time, struct time to_time) {
  unsigned long from_seconds = time_to_seconds(from_time);
  unsigned long to_seconds = time_to_seconds(to_time);
  unsigned long diff_seconds;
  if (from_seconds >= to_seconds) {
    diff_seconds = from_seconds - to_seconds;
  } else {
    diff_seconds = to_seconds - from_seconds;
  }
  return seconds_to_time(diff_seconds);
}
