// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Unit firmware

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//                        interval
//                       |<------->||< offset * unit number            period
//                       |<----------------------------------------------------------------------------->|
//
// Unit 1  ---|----------|---------|---------|---------|---------|---------|---------|---------|---------|---------|-----
// Unit 2  ----|---------|----------|----------|----------|----------|--------|--------|--------|--------|----------|----
// Unit 3  -----|--------|-----------|-----------|-----------|-----------|-------|-------|-------|-------|-----------|---
//                       ^                                       ^                                       ^
//                     Sync                            Midpoint (spread out evenly)                     Sync
//                                 Increasing Offset                            Decreasing offset
//                                 multiplied by Unit number                    multiplied by Unit number
//
// setting_period_min: Sync every n mins
// Requirement: needs to be divisible in 24h
// (24 * 60) % setting_period_min == 0
//
// setting_interval_sec: Flash every n secs:
// requirement: divisible evenly in period
// ((setting_period_min * 60) / setting_interval_sec) % 2 == 0
// 
// Offset: 
// period_millis = setting_period_min * 60 * 1000 (max 24h: 86'400'000 -> unsigned long)
// interval_millis = setting_interval_sec * 1000 (max 12h: 43'200'000 -> unsigned long)
// num_intervals = setting_period_min * 60 / setting_interval_sec (max 24h/1s: 86'400 -> unsigned long)
// general_offset_millis = interval_millis / total_units / number_intervals (max 10'800'000 -> unsigned long)
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

// PINS

static const int PIN_BUTTON_MODE_A = 2;
static const int PIN_BUTTON_MODE_B = 3;
static const int PIN_BUTTON_MODE_LED = 4;

static const int PIN_BUTTON_ROTARY_A = 5; // Rotary CLK
static const int PIN_BUTTON_ROTARY_B = 6; // Rotary DT
static const int PIN_BUTTON_ROTARY_PUSH = 7; // Rotary SW

static const int PIN_GPS_PPS = 8;
// static const int PIN_GPS_TX = 9;
// static const int PIN_GPS_RX = 10;

static const int PIN_FLASH = 11;
static const int PIN_FLASH_LED = 12;

// Display pins (no need to define): 
// SDA = 18
// SCL = 19

// GLOBAL VARIABLES
// Settings
byte setting_unit_number = 45;
byte setting_total_units = 50;
unsigned int setting_period_min = 10;
unsigned long setting_interval_sec = 12;
// Timing
char mode;  // 'c' = Config, 'd' = Demo, 'r' = Run
int next_offset_multiplier = 0;
unsigned int current_millis;
struct time {
  unsigned int hh = 0;
  unsigned int mm = 0;
  unsigned int ss = 0;
};
struct time utc_time;
struct time last_utc_time;
struct time next_trigger_time;
struct time setting_period_time;
struct time setting_interval_time;
// GPS
unsigned int gps_satellites;
bool is_gps_available = false;
bool is_gps_updated = false;
// Display
bool is_display_outdated = true;
int config_step = 0;
// Flash
bool is_flash_ready = false;
unsigned int scheduled_offset_millis;

void setup() {
  display_setup();
  comm_setup();
  flash_setup();
  button_setup();
  gps_setup();
  led_setup();
  config_setup();
}

void loop() {
  current_millis = millis();
  flash_trigger_handler();
  gps_handler();
  flash_schedule_handler();
  button_handler();
  led_handler();
  comm_receive();  // Debug
  if (is_display_outdated) {
    display_update();
  }
}
