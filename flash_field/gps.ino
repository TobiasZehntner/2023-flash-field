// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// GPS

#include <TinyGPSPlus.h>
// #include <SoftwareSerial.h>

static const uint32_t GPS_BAUD_RATE = 9600;

TinyGPSPlus gps;
// SoftwareSerial software_serial(PIN_GPS_RX, PIN_GPS_TX);
unsigned int gps_last_signal = 0;

void gps_setup() {
  pinMode(PIN_GPS_PPS, INPUT);
  attachInterrupt(digitalPinToInterrupt(PIN_GPS_PPS), pps_interrupt_handler, RISING);
  // software_serial.begin(GPS_BAUD_RATE);
  Serial1.begin(GPS_BAUD_RATE);
}

void gps_handler() {
  if (is_gps_available && (current_millis - gps_last_signal) > 2000) {
    // No second-pulse for more than 2 seconds
    is_gps_available = false;
    is_display_outdated = true;
  }
  if (is_gps_available && !is_gps_updated) {
    // Read time once after PPS signal
    gps_read();
  }
}

void gps_read() {
  // while (software_serial.available() > 0) {
  while (Serial1.available() > 0) {
    // if (gps.encode(software_serial.read())) {
    if (gps.encode(Serial1.read())) {
      if (gps.time.isValid()) {
        byte new_hh = gps.time.hour();
        byte new_mm = gps.time.minute();
        byte new_ss = gps.time.second();
        if (last_utc_time.ss != new_ss || last_utc_time.mm != new_mm || last_utc_time.hh != new_hh) {
          // Update time
          utc_time.hh = new_hh;
          utc_time.mm = new_mm;
          utc_time.ss = new_ss;

          last_utc_time.hh = new_hh;
          last_utc_time.mm = new_mm;
          last_utc_time.ss = new_ss;
          is_gps_updated = true;
        }
      }
      if (gps.satellites.isValid()) {
        gps_satellites = gps.satellites.value();
      }
    }
  }
}

void pps_interrupt_handler() {
  utc_time = increase_time(utc_time, 1);
  schedule_flash();
  is_gps_available = true;
  is_gps_updated = false;
  is_display_outdated = true;
  gps_last_signal = current_millis;
}
