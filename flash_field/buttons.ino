// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Buttons

const int DEBOUNCE_DELAY = 10;

bool button_state_mode_a = LOW;
bool button_state_mode_b = LOW;
bool button_state_rotary_a = LOW;
bool button_state_rotary_press = LOW;

bool prev_button_state_mode_a = LOW;
bool prev_button_state_mode_b = LOW;
bool prev_button_state_rotary_a = LOW;
bool prev_button_state_rotary_press = LOW;

unsigned int debounce_millis = 0;
unsigned int debounce_rotary_plus = 0;
unsigned int debounce_rotary_minus = 0;

void button_setup() {
  pinMode(PIN_BUTTON_MODE_A, INPUT_PULLUP);
  pinMode(PIN_BUTTON_MODE_B, INPUT_PULLUP);
  pinMode(PIN_BUTTON_MODE_LED, OUTPUT);

  pinMode(PIN_BUTTON_ROTARY_A, INPUT);
  pinMode(PIN_BUTTON_ROTARY_B, INPUT);
  pinMode(PIN_BUTTON_ROTARY_PUSH, INPUT_PULLUP);
}

void button_handler() {
  bool new_button_state_mode_a = digitalRead(PIN_BUTTON_MODE_A);
  bool new_button_state_mode_b = digitalRead(PIN_BUTTON_MODE_B);
  bool new_button_state_rotary_a = digitalRead(PIN_BUTTON_ROTARY_A);
  bool new_button_state_rotary_press = digitalRead(PIN_BUTTON_ROTARY_PUSH);

  if (
    new_button_state_mode_a != prev_button_state_mode_a || new_button_state_mode_b != prev_button_state_mode_b || new_button_state_rotary_press != prev_button_state_rotary_press) {
    // Change in buttons, reset timer
    debounce_millis = current_millis;
  }

  if ((current_millis - debounce_millis) > DEBOUNCE_DELAY) {
    // If timer has passed and buttons are still different, save correct state and call actions
    if (new_button_state_mode_a != button_state_mode_a || new_button_state_mode_b != button_state_mode_b) {
      button_state_mode_a = new_button_state_mode_a;
      button_state_mode_b = new_button_state_mode_b;

      if (button_state_mode_a == HIGH && button_state_mode_b == LOW) {
        // Mode: Config
        action_mode('c');
      } else if (button_state_mode_a == HIGH && button_state_mode_b == HIGH) {
        // Mode: Demo
        action_mode('d');
      } else {
        // Mode: Run
        action_mode('r');
      }
    }
    if (mode == 'c') {
      if (new_button_state_rotary_press != button_state_rotary_press) {
        button_state_rotary_press = new_button_state_rotary_press;
        if (button_state_rotary_press == LOW) {
          action_enter();
        }
      }
    }
  }

  // Seperate debounce for rotary: debounce reverse direction to not fall back, but count all clicks in same direction
  if (mode == 'c' && new_button_state_rotary_a != prev_button_state_rotary_a && new_button_state_rotary_a == HIGH) {
    button_state_rotary_a = new_button_state_rotary_a;
    if ((current_millis - debounce_rotary_minus) > DEBOUNCE_DELAY && digitalRead(PIN_BUTTON_ROTARY_B) != new_button_state_rotary_a) {
      action_plus();
      debounce_rotary_plus = current_millis;
    } else if ((current_millis - debounce_rotary_plus) > DEBOUNCE_DELAY) {
      action_minus();
      debounce_rotary_minus = current_millis;
    }
  }

  // Save last recorded state (even if faulty)
  prev_button_state_mode_a = new_button_state_mode_a;
  prev_button_state_mode_b = new_button_state_mode_b;
  prev_button_state_rotary_a = new_button_state_rotary_a;
  prev_button_state_rotary_press = new_button_state_rotary_press;
}


void action_mode(char new_mode) {
  mode = new_mode;
  if (mode == 'c') {
    config_step = 0;
  }
  is_flash_ready = false;
  is_display_outdated = true;
}
void action_minus() {
  iterate_config(-1);
  is_display_outdated = true;
}
void action_plus() {
  iterate_config(1);
  is_display_outdated = true;
}
void action_enter() {
  iterate_config_step();
  is_display_outdated = true;
}
