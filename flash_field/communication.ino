// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Communication

const unsigned long SERIAL_BAUD_RATE = 500000;

void comm_setup() {
  Serial.begin(SERIAL_BAUD_RATE);
  Serial.println("Setting up Flash Field");
}
void comm_receive() {
  if (Serial.available() > 0) {
    int command = Serial.read();
    comm_data_handler(command);
  }
}
void comm_data_handler(int command) {
  switch (command) {
    case 'a':
      Serial.println("Read GPS");
      gps_read();
      break;
    case 'd':
      Serial.println("Update Display");
      display_update();
    case 't':
      Serial.println("Print Time");
      print_time();
    case 'b':
      button_handler();
      Serial.print("Print Button State: ");
      Serial.print(button_state_mode_a);
      Serial.print(" / ");
      Serial.print(button_state_mode_b);
      Serial.println();
    default:
      break;
  }
}

void print_time() {
  Serial.print(utc_time.hh);
  Serial.print(":");
  Serial.print(utc_time.mm);
  Serial.print(":");
  Serial.print(utc_time.ss);
  Serial.print(" (Satellites: ");
  Serial.print(gps_satellites);
  Serial.print(")");
  Serial.println();
}