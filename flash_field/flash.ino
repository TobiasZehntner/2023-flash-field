// © 2024 Tobias Zehntner
// License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).

// Artwork: Flash Field (2024)
// Flash

#include "digitalWriteFast.h"

int FLASH_PULSE_WIDTH_MICROS = 1000;  // Length of trigger pulse in microseconds

unsigned int flash_scheduled_millis;
bool is_flash_scheduled = false;

void flash_setup() {
  pinMode(PIN_FLASH, OUTPUT);
}
void flash_trigger() {
  digitalWriteFast(PIN_FLASH, HIGH);
  delayMicroseconds(FLASH_PULSE_WIDTH_MICROS);
  digitalWriteFast(PIN_FLASH, LOW);
  is_flash_ready = false;
}
void flash_trigger_handler() {
  if (is_flash_scheduled && current_millis >= flash_scheduled_millis) {
    flash_trigger();
    is_flash_scheduled = false;
  }
}
void flash_schedule_handler() {
  if (mode != 'c' && is_gps_available && is_gps_updated && !is_flash_ready && !is_flash_scheduled) {
    set_next_trigger();
  }
}
void schedule_flash() {
  if (is_flash_ready && utc_time.hh == next_trigger_time.hh && utc_time.mm == next_trigger_time.mm && utc_time.ss == next_trigger_time.ss) {
    if (scheduled_offset_millis == 0) {
      // Sync: trigger flash directly
      flash_trigger();
    } else {
      // Schedule trigger in main loop
      flash_scheduled_millis = millis() + scheduled_offset_millis;
      is_flash_scheduled = true;
    }
  }
}

void set_next_trigger() {
  unsigned long current_total_seconds = time_to_seconds(utc_time);
  unsigned long last_trigger_seconds = (current_total_seconds / setting_interval_sec) * setting_interval_sec;  // division to int
  unsigned long next_trigger_seconds = last_trigger_seconds + setting_interval_sec;

  int num_trigger_total = next_trigger_seconds / setting_interval_sec;
  int num_period_intervals = setting_period_min * 60L / setting_interval_sec;
  int next_interval_num = num_trigger_total % num_period_intervals;
  int next_offset_multiplier;

  if (mode == 'd') {
    // Demo mode: toggle between sync and max offset
    if (next_interval_num % 2 == 0) {
      next_offset_multiplier = 0;
    } else {
      next_offset_multiplier = num_period_intervals / 2;
    }
  } else {
    // Run mode: offset
    // Offset: at half time, the units should be spread out evenly between two intervals: flash every 0.2s for 50 units in 10s
    if (next_interval_num <= num_period_intervals / 2) {
      // Increase offset up to half-time of period
      next_offset_multiplier = next_interval_num;
    } else {
      // Decrease offset until sync
      next_offset_multiplier = (num_period_intervals / 2) - (next_interval_num % (num_period_intervals / 2));
    }
  }
  unsigned long period_millis = setting_period_min * 60 * 1000;
  unsigned long interval_millis = setting_interval_sec * 1000;
  unsigned long num_intervals = setting_period_min * 60 / setting_interval_sec;
  unsigned long general_offset_millis = interval_millis / setting_total_units / num_intervals;
  unsigned long total_scheduled_offset_millis = next_offset_multiplier * general_offset_millis * (setting_unit_number - 1);
  unsigned long offset_ss = total_scheduled_offset_millis / 1000;
  scheduled_offset_millis = total_scheduled_offset_millis % 1000;
  next_trigger_time = increase_time(seconds_to_time(next_trigger_seconds), offset_ss);
  is_flash_ready = true;
}
