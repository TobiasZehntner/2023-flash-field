# Flash Field (2024)

## Settings
### Schema
```
                       interval
                      |<------->||< offset * unit number            period
                      |<----------------------------------------------------------------------------->|

Unit 1  ---|----------|---------|---------|---------|---------|---------|---------|---------|---------|---------|-----
Unit 2  ----|---------|----------|----------|----------|----------|--------|--------|--------|--------|----------|----
Unit 3  -----|--------|-----------|-----------|-----------|-----------|-------|-------|-------|-------|-----------|---
                      ^                                       ^                                       ^
                    Sync                            Midpoint (spread out evenly)                     Sync
                                Increasing Offset                            Decreasing offset
                                multiplied by Unit number                    multiplied by Unit number
```
### Requirements
- `setting_period_min`: Sync every n mins
	- Requirement: needs to be divisible in 24h
	- `(24 * 60) % setting_period_min == 0`
- `setting_interval_sec`: Flash every n secs:
	- Requirement: divisible evenly in period
	- `((setting_period_min * 60) / setting_interval_sec) % 2 == 0`

## Hardware
### Rotary Button
- [How to](https://lastminuteengineers.com/rotary-encoder-arduino-tutorial/)

## Versions
- `0.1` Working version with Arduino Mega and push/rotary buttons