
int PIN_FLASH_1 = LED_BUILTIN;
int PIN_FLASH_2 = 7;


int FLASH_PULSE_WIDTH_MICROS = 1000;

int interval_1_millis = 12000;
int interval_2_millis = 12010;
unsigned long previousMillis_1 = 0; 
unsigned long previousMillis_2 = 0; 

void setup() {
  pinMode(PIN_FLASH_1, OUTPUT);
  pinMode(PIN_FLASH_2, OUTPUT);
}

void loop() {
  unsigned long currentMillis = millis();

  if (currentMillis - previousMillis_1 >= interval_1_millis) {
    previousMillis_1 = currentMillis;

    digitalWrite(PIN_FLASH_1, HIGH);
    delayMicroseconds(FLASH_PULSE_WIDTH_MICROS);
    digitalWrite(PIN_FLASH_1, LOW);
  }

  if (currentMillis - previousMillis_2 >= interval_2_millis) {
    previousMillis_2 = currentMillis;

    digitalWrite(PIN_FLASH_2, HIGH);
    delayMicroseconds(FLASH_PULSE_WIDTH_MICROS);
    digitalWrite(PIN_FLASH_2, LOW);
  }

}
